The Milestone contained was made entirely by Jorden Tischer for his current CST-117 course, submitted on April 1st, 2017.
Contained within the milestone is a Visual Studio windows application of the rough layout of the final project, featuring two tabs. The first contains an exit button and a "view inventory" button
the purpose of each is to respectively close the program and enter the inventory, however I have no idea how to set buttons to change scenes or exit, so I just created multiple tabs with 
the second tab containing what the inventory should resemble. It is set by a sql table containing the book's title, current stock, and maximum stock. in the future, there will be several 
inputs availiable allowing for these to be changed and having an effect on other columns through these changes (such as setting max stock to 0 deleting the row), but again, drawing 
several blanks. There is also a hidden "ID" value that I would like to set to auto-increment like it can in phpmyadmin, but until then the ability to add books to the directory is 
unavailable. The final tab is a planned "Bookinfo" page which contains more information about the book to display, though this will be the biggest challenge and has a high chance 
of being absent from the final product if time and programming knowledge never gets to that point.