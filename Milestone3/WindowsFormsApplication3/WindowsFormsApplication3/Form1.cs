﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

                Console.WriteLine("Book Directory");
                Console.WriteLine(" ");
                for (int i = 0; i < 50; i++)
                {
                    int book = i + 1;
                    Console.Write("|View Info| |Delete| " + book);
                    Console.WriteLine(". BookTitle#" + book);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'practiceInventoryDataSet.Table' table. You can move, or remove it, as needed.
            this.tableTableAdapter.Fill(this.practiceInventoryDataSet.Table);

        }
    }

}
    
