﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//This is my own work: Jorden Tischer
namespace CodeBlock
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = System.IO.File.ReadAllText(@"C:\CST-117\ConsoleApplication11\sentence.txt");
            char[] delimiters = { ' ', '\n', '\r', '\t', ',', '.', '?', '!', '(', ')', '"'};
            string[] words = sentence.ToLower().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            Array.Sort(words); //Credit for this goes to a random guy on Stack Overflow, also used .ToLower() method in order to efficiently sort words
            int v = 0;
            for (int i = 0; i < words.Length; i++)
            {
                char[] firstLetter = words[i].ToCharArray(); //how I learned to split words into characters credited to Isley Autrey
                if (firstLetter[0] == 'a')
                {

                    v++;
                }
                else if (firstLetter[0] == 'e')
                {
                    v++;
                }
                else if (firstLetter[0] == 'i')
                {
                    v++;
                }
                else if (firstLetter[0] == 'o')
                {
                    v++;
                }
                else if (firstLetter[0] == 'u')
                {
                    v++;
                }
            }
            int highest = 0;
            String longest = "new word here";
            for (int i = 0; i < words.Length; i++)
            {
                char[] letters = words[i].ToCharArray();
                if (letters.Length == highest)
                {
                    longest = longest + "/" + words[i];
                }
                else if (letters.Length > highest)
                {
                    highest = letters.Length;
                    longest = words[i];
                }
            }
            Console.WriteLine("Your sentence: IT'S NOT A SENTENCE IT'S BOOK ONE OF THE ODYSSEY!");
            Console.WriteLine("First Word Alphabetically: " + words[0]);
            Console.WriteLine("Last Word: " + words[words.Length - 1]);
            Console.WriteLine("Number of words starting with vowels: " + v);
            Console.WriteLine("Longest word: " + longest);
            Console.WriteLine("Total number of words: " + words.Length);
        }
    }
}
