﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication10
{
    class Program
    {
        static void Main(string[] args)
        {
            Chicken chicken1 = new Chicken("[First]", 3);
            Chicken chicken2 = new Chicken("[Second]", 4);
            Chicken chicken3 = new Chicken("[Third]", 5);
            Console.WriteLine("{0}, {1}, {2}", chicken1, chicken2, chicken3);
            //calls the whole house
            ChickenHouse cHouse = new ChickenHouse();
            Console.WriteLine(cHouse);
            cHouse.ChickenDisplay();
            Console.WriteLine(cHouse);
        }
    }
    class Chicken
    {
        private String name;
        private int monthsOld;
        public Chicken(string Name, int age)
        {
            this.name = Name;
            this.monthsOld = age;
        }
        public string getName() { return this.name; }
        public int getAge() { return this.monthsOld; }
        public void setName(string n)
        {
            this.name = n;
        }
        public void setAge(int age)
        {
            this.monthsOld = age;
        }
        public override string ToString()
        {
            return this.name;
        }
    }

    class ChickenHouse
    {
        private Chicken[] ___;
        public ChickenHouse(){
            ___ = new Chicken[6];
            ___[0] = new Chicken("[Mr. Chicken]", 3);
            ___[1] = ___[2] = ___[4] = null;
            ___[3] = new Chicken("[Mrs. Chicken]", 4);
            ___[5] = new Chicken("[Ms. Chicken]", 5);
        }
        public override string ToString()
        {
            String str = "  ";
            for (int i = 0; i < ___.Length; i++)
            {
                if (___[i] == null)
                    str += "N/A\t";
                else
                    str += ___[i] + "\t";
            }
            return str;
        }
        public int findChicken(string name) { return -1; }
        public void ChickenDisplay()
        {
            for (int empty = 0; empty < ___.Length; empty++)
            {
                if (___[empty] == null)
                {
                    for (int chick = empty; chick < ___.Length; chick++)
                    {
                        if (___[chick] != null)
                        {
                            ___[empty] = ___[chick];
                            ___[chick] = null;
                            break;
                        }

                    }
                }

            }

        }
    }
}


