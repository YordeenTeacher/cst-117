﻿using System;

public class Fraction
{
    //define the data
    private int numerator;
    private int denominator;

    public int Numerator
    {
        get
        {
            return numerator;
        }

        set
        {
            numerator = value;
        }
    }

    public int Denominator
    {
        get
        {
            return denominator;
        }

        set
        {
            denominator = value;
        }
    }

    //define a constructor and ways to access/update the data

    public Fraction(int numerator, int denominator)
    {
        this.Numerator = numerator;
        this.Denominator = denominator;
    }
    public Fraction multiply(Fraction rhs)
    {
        Fraction answer = new Fraction(this.numerator, this.denominator);
        answer.Numerator = this.numerator * rhs.numerator;
        answer.Denominator = this.denominator * rhs.denominator;
        return answer;
    }
    public Fraction divide (Fraction rhs)
    {
        Fraction answer = new Fraction(this.numerator, this.denominator);
        //multiply by recipricoal
        answer.Numerator *= rhs.Denominator;
        answer.Denominator *= rhs.Numerator;
        return answer;
    }
}
